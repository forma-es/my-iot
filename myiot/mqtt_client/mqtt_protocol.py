import paho.mqtt.client as mqtt
from django.utils import timezone, dateformat

import myiot.settings as cfg


data = {}

def on_connect(client, userdata, flags, rc):
    if rc == 0: #ok
        print('Connection OK')
        client.subscribe('esp32/+/sensors/#')
        client.message_callback_add('esp32/#', on_message)
    else:
        print('Connection fail')

def on_message(client, userdata, msg):
    #print(msg.topic, '\t', float(msg.payload)) # DEBUG
    sensor = msg.topic.split('/')[3]
    value = float(msg.payload)
    timestamp = dateformat.format(timezone.now(), 'Y-m-d H:i:s')
    data[sensor] = value
    data['timestamp'] = timestamp
    #print(data) # DEBUG
    

    #Add History
    if sensor == 'temperature':
        from .models import Temperature
        t = Temperature(timestamp=timezone.now(), value=value)
        t.save()


client = mqtt.Client()
client.on_connect = on_connect
client.username_pw_set(cfg.MQTT_USER, cfg.MQTT_PASSWORD)
client.connect(cfg.MQTT_SERVER, port=1883)
#client.loop_forever()

