from django.db import models

class Temperature(models.Model):
    timestamp = models.DateTimeField()
    value = models.FloatField()

    def __str__(self) :
        return f"{self.timestamp} \t {self.value}"
