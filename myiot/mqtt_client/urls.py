from django.urls import path
from . import views

app_name = 'sensors'

urlpatterns = [
    path("", views.get_data, name='realtime'),
    path("temperature/", views.get_temperature, name='temperature'),
]