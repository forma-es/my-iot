from django.shortcuts import render
from django.http import JsonResponse
from .models import Temperature

from mqtt_client.mqtt_protocol import data

def get_data(request):
    return JsonResponse(data)

def get_temperature(request):
    last_temperature = Temperature.objects.order_by('-timestamp')[:20]
    output = {'temperatures': [{'timestamp': t.timestamp, 'value': t.value}
                               for t in last_temperature]}
    return JsonResponse(output)