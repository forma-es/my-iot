FROM python:3.10-alpine 

# Set environment variables 
ENV PIP_DISABLE_PIP_VERSION_CHECK 1 
ENV PYTHONDONTWRITEBYTECODE 1 
ENV PYTHONUNBUFFERED 1 

# Create a user and don't use root 
RUN adduser -D user 
USER user 
WORKDIR /home/user 
ENV PATH="/home/user/.local/bin:${PATH}" 

# Install dependencies 
COPY --chown=user:user requirements.txt . 
RUN pip install --user -r requirements.txt 

# Copy remain software 
COPY --chown=user:user . . 

# Port used 
EXPOSE 8000 

CMD "cd myiot && gunicorn myiot.wsgi"